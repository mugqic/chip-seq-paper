source("packages.R")

load("chunks.RData")

ann.colors <-
  c(noPeaks="#f6f4bf",
    peakStart="#ffafaf",
    peakEnd="#ff4c4c",
    peaks="#a445ee")

sets <- split(chunks, with(chunks, list(sample.set, experiment)))
set <- sets$immune.H3K36me3
ann.list <- split(set, set$annotator, drop=TRUE)
ch <- function(AM, TDH, expandStart, expandEnd){
  data.frame(AM, TDH, expandStart, expandEnd)
}
chunk.info <-
  rbind(ch(20, 1, 127900, 128800),
        ch(21, 2, 116000, 117000),
        ch(21, 2, 118500, 119500),
        ch(22, 3, 18500, 19500),
        ch(22, 3, 19800, 20900),
        ch(22, 3, 21500, 22500),
        ch(23, 4, 15600, 16600))
for(chunk.i in 1:nrow(chunk.info)){
  r <- chunk.info[chunk.i, ]
  all.regions <- list()
  for(annotator in c("AM", "TDH")){
    chunk.id.str <- paste(r[[annotator]])
    ann.df <- ann.list[[annotator]]
    rownames(ann.df) <- ann.df$chunk.id
    meta <- ann.df[chunk.id.str, ]
    chunk.dir <- with(meta, file.path("chunks", set.name, chunk.id))
    load(file.path(chunk.dir, "signal.RData"))
    load(file.path(chunk.dir, "regions.RData"))
    all.regions[[annotator]] <- regions
  }
  stopifnot(as.character(all.regions[[1]]$chrom[1]) ==
            as.character(all.regions[[2]]$chrom[1]))
  chunkChrom <- as.character(regions$chrom[1])
  expandStart <- r$expandStart*1e3
  expandEnd <- r$expandEnd*1e3
  sample.ids <- as.character(unique(regions$sample.id))
  samples <- length(sample.ids)
  target.w <- (expandEnd-expandStart)/5e4
  w <- min(100, max(10, target.w))
  target.h <- samples/2
  h <- min(20, max(5, target.h))
  png.name <- sprintf("figure-several-annotators-%d.png", chunk.i)

  png(png.name, height=h, width=w, units="in", res=200)
  border <- "black"
  peakLines <- 
    ggplot()+
      geom_line(aes(chromStart/1e3, coverage),
                data=signal, color="grey50")+
      ggtitle("top=AM bottom=TDH")+
      geom_tallrect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
                        fill=annotation),
                    data=all.regions$TDH,
                    ymax=1/2, alpha=1/2, color=border)+
      geom_tallrect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
                        fill=annotation),
                    data=all.regions$AM,
                    ymin=1/2, alpha=1/2, color=border)+
      scale_fill_manual("annotation", values=ann.colors,
                        breaks=names(ann.colors))+
      theme_bw()+
      theme(panel.margin=grid::unit(0, "cm"))+
      facet_grid(cell.type + sample.id ~ ., scales="free",
                 labeller=function(var, val){
                   sub("McGill0", "", val)
                 })+
      scale_y_continuous("normalized coverage from bigWig files",
                         labels=function(x){
                           sprintf("%.1f", x)
                         },
                         breaks=function(limits){
                           limits[2]
                         })+
      xlab(sprintf("position on %s (kilo base pairs)", chunkChrom))+
      coord_cartesian(xlim=c(expandStart, expandEnd)/1e3)

  print(peakLines)
  dev.off()

  if(chunk.i == 2){
    png("figure-several-annotators.png",
        height=5, width=11, units="in", res=200)
    only <- function(df){
      subset(df, sample.id=="McGill0004")
    }
    one.profile <- list()
    one.region <- list()
    for(annotator in names(all.regions)){
      profile <- only(all.regions[[annotator]])
      one.profile[[annotator]] <- profile
      one.region[[annotator]] <- subset(profile, chromStart==min(chromStart))
    }
    peakLines <- 
    ggplot()+
      geom_line(aes(chromStart/1e3, coverage),
                data=only(signal), color="grey50")+
      geom_tallrect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
                        fill=annotation),
                    data=one.profile$TDH,
                    ymax=1/2, alpha=1/2, color=border)+
      geom_tallrect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
                        fill=annotation),
                    data=one.profile$AM,
                    ymin=1/2, alpha=1/2, color=border)+
      geom_text(aes(chromStart/1e3),
                    data=one.region$TDH,
                    y=3, label="annotator TDH ", hjust=1)+
      geom_text(aes(chromStart/1e3),
                    data=one.region$AM,
                    y=6, label="annotator AM ", hjust=1)+
      scale_fill_manual("annotation", values=ann.colors,
                        breaks=names(ann.colors))+
      theme_bw()+ 
      scale_y_continuous("normalized coverage from bigWig files")+
      xlab(sprintf("position on %s (kilo base pairs)", chunkChrom))+
      coord_cartesian(xlim=c(expandStart, expandEnd)/1e3)

    print(peakLines)
    dev.off()
  }
}
