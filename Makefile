HOCKING-bioinfo.pdf: HOCKING-bioinfo.tex refs.bib
	rm -f *.aux *.bbl
	pdflatex HOCKING-bioinfo
	bibtex HOCKING-bioinfo
	pdflatex HOCKING-bioinfo
	pdflatex HOCKING-bioinfo
# HOCKING-bioinfo.pdf: HOCKING-bioinfo.tex refs.bib
# 	rm -f *.aux *.bbl
# 	latex HOCKING-bioinfo
# 	bibtex HOCKING-bioinfo
# 	latex HOCKING-bioinfo
# 	latex HOCKING-bioinfo
# 	dvipdf HOCKING-bioinfo
HOCKING-nar.pdf: HOCKING-nar.tex refs.bib screenshot-annotations-patricia.eps figure-PeakError.eps figure-train-error.png figure-roc-test.png figure-test-error-decreases.png nar.bst
	rm -f *.aux *.bbl
	pdflatex HOCKING-nar
	bibtex HOCKING-nar
	pdflatex HOCKING-nar
	pdflatex HOCKING-nar
nar.bst:
	wget https://www.ctan.org/tex-archive/biblio/bibtex/contrib/misc/nar.bst
HOCKING-bmc.pdf: HOCKING-bmc.tex refs.bib screenshot-annotations-patricia.eps figure-PeakError.eps figure-train-error.png figure-roc-test.png figure-test-error-decreases.png 
	rm -f *.aux *.bbl
	pdflatex HOCKING-bmc
	bibtex HOCKING-bmc
	pdflatex HOCKING-bmc
	pdflatex HOCKING-bmc
HOCKING-bmc-supp.pdf: HOCKING-bmc-supp.tex figure-learned-1.png figure-learned-2.png figure-test-error-dots-all.pdf figure-roc-test-all.pdf figure-test-H3K4me3-annotators.pdf figure-several-annotators.png figure-test-H3K4me3-types.pdf figure-test-TDH-experiments.pdf
	rm -f *.aux *.bbl
	pdflatex HOCKING-bmc-supp
	bibtex HOCKING-bmc-supp
	pdflatex HOCKING-bmc-supp
	pdflatex HOCKING-bmc-supp
HOCKING-plos.pdf: HOCKING-plos.tex refs.bib plos2015.bst screenshot-annotations-patricia.eps figure-PeakError.eps figure-train-error.png figure-roc-test.png figure-test-error-decreases.png 
	rm -f *.aux *.bbl
	latex HOCKING-plos
	bibtex HOCKING-plos
	latex HOCKING-plos
	latex HOCKING-plos
	dvipdf HOCKING-plos
HOCKING-chip-seq-slides.pdf: HOCKING-chip-seq-slides.tex figure-splits.pdf
	rm -f *.aux *.bbl
	pdflatex HOCKING-plos
figure-splits.pdf: figure-splits.R roc.test.RData
	R --no-save < $<
PeakSegJoint.problems.txt: PeakSegJoint.problems.R dp.peaks.regression.RData
	R --no-save < $<
screenshot-annotations-patricia.eps: screenshot-annotations-patricia.pdf
	pdf2ps screenshot-annotations-patricia.pdf screenshot-annotations-patricia.eps
figure-PeakError.eps: figure-PeakError.pdf
	pdf2ps figure-PeakError.pdf figure-PeakError.eps
HOCKING-peak-annotations.pdf: HOCKING-peak-annotations.tex refs.bib figure-PeakError.pdf screenshot-annotations-fig.pdf figure-train-error.pdf figure-roc-test.png table-annotation-types.tex supplementary.tex figure-test-same.pdf
	rm -f *.aux *.bbl
	pdflatex HOCKING-peak-annotations
	bibtex HOCKING-peak-annotations
	pdflatex HOCKING-peak-annotations
	pdflatex HOCKING-peak-annotations
HOCKING-supplementary.pdf: HOCKING-supplementary.tex figure-train-error.pdf figure-roc.pdf supplementary.tex figure-several-annotators.png figure-test-annotators.pdf figure-learned-1.png
	rm -f *.aux *.bbl
	pdflatex HOCKING-supplementary
	bibtex HOCKING-supplementary
	pdflatex HOCKING-supplementary
	pdflatex HOCKING-supplementary
## These two make the chunks/ database.
chunks/srf_NTNU_Gm12878/437/regions.RData: TF.benchmark.corrected.R
	R --no-save < $<
chunks.RData: chunks.R
	R --no-save < $<
## These read algo-specific peak files and translate them to RData files.
unsupervised.error.RData: unsupervised.error.R unsupervised.RData dp.peaks.sets.RData
	R --no-save < $<
unsupervised.RData: unsupervised.R
	R --no-save < $<
figure-roc-test.png: figure-roc-test.R roc.test.RData pdf.png.R
	R --no-save < $<
roc.test.RData: roc.test.R dp.peaks.regression.RData error.regions.RData algo.colors.R unsupervised.error.RData
	R --no-save < $<
figure-test-error-decreases.png: figure-test-error-decreases.R test.error.decreases.RData roc.test.RData pdf.png.R
	R --no-save < $<
test.error.other.RData: test.error.other.R pick.best.index.R train.PeakSeg.R dp.peaks.intervals.RData dp.peaks.sets.RData
	R --no-save < $<
test.error.decreases.RData: test.error.decreases.R pick.best.index.R train.PeakSeg.R dp.peaks.intervals.RData dp.peaks.sets.RData
	R --no-save < $<
dp.peaks.regression.RData: dp.peaks.regression.R dp.peaks.intervals.RData dp.peaks.sets.RData
	R --no-save < $<
dp.peaks.intervals.RData: dp.peaks.intervals.R dp.peaks.matrices.RData dp.peaks.features.RData
	R --no-save < $<
dp.peaks.features.RData: dp.peaks.features.R
	R --no-save < $<
dp.peaks.optimal.RData: dp.peaks.optimal.R dp.peaks.matrices.RData
	R --no-save < $<
dp.peaks.sets.RData: dp.peaks.sets.R dp.peaks.matrices.RData
	R --no-save < $<
dp.peaks.matrices.RData: dp.peaks.matrices.R dp.peaks.error.RData
	R --no-save < $<
dp.peaks.error.RData: dp.peaks.error.R dp.peaks.RData
	R --no-save < $<
dp.peaks.RData: dp.peaks.R dp.timings.RData
	R --no-save < $<
dp.timings.RData: dp.timings.R
	R --no-save < $<
chunks/H3K4me3_TDH_immune/5/peaks/homer.default.RData: homer.R chunks.RData
	R --no-save < $<
chunks/H3K4me3_TDH_immune/5/peaks/sicer.default.RData: sicer.R chunks.RData
	R --no-save < $<
chunks/H3K4me3_TDH_immune/5/peaks/hmcan.default.RData: hmcan.R chunks.RData
	R --no-save < $<
chunks/H3K4me3_TDH_immune/5/peaks/rseg.default.RData: rseg.R chunks.RData
	R --no-save < $<
chunks/H3K4me3_TDH_immune/5/peaks/macs.default.RData: macs.R chunks.RData
	R --no-save < $<
chunks/H3K4me3_TDH_immune/5/peaks/ccat.tf.peak.default.RData: ccat.R chunks.RData
	R --no-save < $<
triform.error.RData: triform.error.R TF.benchmark.corrected.RData chunks.RData
	R --no-save < $<
figure-triform-error.pdf: figure-triform-error.R triform.error.RData
	R --no-save < $<
## Compute error/algo.RData files for each data set and algo.
error.regions.RData: error.regions.R chunks/H3K4me3_TDH_immune/5/error/hmcan.default.RData
	R --no-save < PeakError.R
	R --no-save < $<
figure-PeakError.pdf: figure-PeakError.R error.regions.RData
	R --no-save < $<
figure-train-error.png: figure-train-error.R error.regions.RData pdf.png.R
	R --no-save < $<
figure-roc.pdf: figure-roc.R error.regions.RData algo.colors.R
	R --no-save < $<
table-annotation-types.tex: table-annotation-types.R error.regions.RData
	R --no-save < $<
figure-several-annotators.png: figure-several-annotators.R chunks.RData
	R --no-save < $<
figure-test-TDH-experiments.png: figure-test-annotators.R test.error.other.RData roc.test.RData algo.colors.R
	R --no-save < $<
chunks/index.html: table-train-errors.R error.regions.RData
	R --no-save < $<
figure-test-same.pdf: figure-test-same.R error.regions.RData algo.colors.R
	R --no-save < $<
figure-learned-1.png: figure-learned.R error.regions.RData
