works_with_R("3.2.3",
             IRanges="2.2.7",
             triform="1.10.0")

params <-
  list(READ.PATH="TF-benchmark",
       COVER.PATH="TF-benchmark",
       READ.WIDTH=100)
preprocess(configPath=NULL, params=params)
Sys.glob("TF-benchmark/*.bed")

MAX.P.values <-
  sort(c(seq(0.1, 0.5, by=0.0125),
         0.05, 0.01, 0.005, 0.001))

chroms <- paste0("chr", c(1:22, "X", "Y"))
for(tf.name in c("srf", "max", "nrsf")){
  bed.paths <- Sys.glob(paste0("TF-benchmark/", tf.name, "_*.bed"))
  TARGETS <- basename(bed.paths)
  print(TARGETS)
  backgr.glob <- sub("rep1", "rep[12]", sub(tf.name, "backgr", bed.paths[1]))
  CONTROLS <- basename(Sys.glob(backgr.glob))
  print(CONTROLS)
  for(MAX.P in MAX.P.values){
    cat(paste(tf.name, MAX.P, "\n"))
    for(chrom in chroms){
      OUTPUT.PATH <-
        sprintf("TF-benchmark/%s_%s_triform_MAX.P=%f.csv",
                chrom, tf.name, MAX.P)
      if(!file.exists(OUTPUT.PATH)){
        params <-
          list(COVER.PATH="TF-benchmark",
               OUTPUT.PATH=OUTPUT.PATH,
               CONTROLS=CONTROLS,
               TARGETS = TARGETS,
               MAX.P = MAX.P, MIN.WIDTH = 10,
               MIN.QUANT = 0.375, MIN.SHIFT = 10, 
               FLANK.DELTA = 150, CHRS = chrom)
        ## Diagnostic:
        params <-
          list(COVER.PATH="TF-benchmark",
               OUTPUT.PATH=OUTPUT.PATH,
               CONTROLS=CONTROLS[1],
               TARGETS = TARGETS[1],
               MAX.P = MAX.P, MIN.WIDTH = 10,
               MIN.QUANT = 0.375, MIN.SHIFT = 10, 
               FLANK.DELTA = 150, CHRS = chrom)
        control.path <- with(params, file.path(COVER.PATH, CONTROLS))
        target.path <- with(params, file.path(COVER.PATH, TARGETS))
        system(paste("wc -l", paste(target.path, control.path, collapse=" ")))
        seconds <- system.time({
          tryCatch({
            fit <- triform(configPath=NULL, params=params)
          }, error=function(e){
            cat("", file=OUTPUT.PATH)
          })
        })[["elapsed"]]
        seconds.path <- sub(".csv$", "_secondsDF.RData", OUTPUT.PATH)
        secondsDF <- data.frame(tf.name, chrom, MAX.P, seconds)
        save(secondsDF, file=seconds.path)
      }
    }
  }
}
