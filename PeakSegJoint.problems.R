works_with_R("3.2.3",
             doParallel="1.0.6",
             "tdhock/PeakError@d9196abd9ba51ad1b8f165d49870039593b94732",
             "tdhock/PeakSegJoint@2f9ed89b4cddca85fccedce42c283950e8f77498",
             data.table="1.9.7",
             "tdhock/ggplot2@a8b06ddb680acdcdbd927773b1011c562134e4d2")

registerDoParallel()

objs <- load("dp.peaks.regression.RData")
objs <- load("dp.peaks.sets.RData")
load("chunks.RData")
rownames(chunks) <- with(chunks, paste0(set.name, "/", chunk.id))

## The script computes a list of PeakSegJoint problem regions for each
## data set using the result of the single-sample PeakSeg algo.

ann.colors <-
  c(noPeaks="#f6f4bf",
    peakStart="#ffafaf",
    peakEnd="#ff4c4c",
    peaks="#a445ee")

genome.pos.pattern <-
  paste0("(?<chrom>chr.*?)",
         ":",
         "(?<chromStart>[0-9]+)",
         "-",
         "(?<chromEnd>[0-9]+)")

set.dir.i <- 5
chunk.id <- "13"

clustered2problems <- function(clustered.df){
  clustered.dt <- data.table(clustered.df)
  clusters <- clustered.dt[, list(
    peakStart=min(chromStart),
    peakEnd=max(chromEnd)
    ), by=cluster]
  setkey(clusters, peakStart, peakEnd)
  clusters[, peakBases := peakEnd - peakStart]
  clusters[, expandStart := peakStart - peakBases]
  clusters[, expandEnd := peakEnd + peakBases]
  clusters[, overlaps.next := c(expandStart[-1] < expandEnd[-.N], FALSE)]
  clusters[, overlaps.prev := c(FALSE, overlaps.next[-.N])]
  clusters[, center.before.next := c((peakStart[-1] + peakEnd[-.N])/2, NA)]
  clusters[, center.after.prev := c(NA, center.before.next[-.N])]
  clusters[, problemStart := as.integer(ifelse(overlaps.prev,
                            center.after.prev, expandStart))]
  clusters[, problemEnd := as.integer(ifelse(overlaps.next,
                          center.before.next, expandEnd))]
  clusters
}  

set.dirs <- Sys.glob("chunks/*_*_*") #include TF data!
for(set.dir.i in seq_along(set.dirs)){
  set.dir <- set.dirs[[set.dir.i]]
  set.name <- basename(set.dir)
  test.set.list <- dp.peaks.sets[[set.name]]
  train.peaks.list <- dp.peaks.regression$train.peaks[[set.name]]
  experiment <- sub("_.*", "", set.name)
  for(testSet.i in seq_along(test.set.list)){
    testSet <- paste(set.name, "split", testSet.i)
    RData.file <- sprintf(
      "PeakSegJoint.problems/%s_split%d.RData", set.name, testSet.i)
    if(!file.exists(RData.file)){
      train.peaks.by.chunk <- train.peaks.list[[testSet]]
      PeakSegJoint.problems <- list()
      for(train.chunk.i in seq_along(train.peaks.by.chunk)){
        train.chunk <- names(train.peaks.by.chunk)[[train.chunk.i]]
        chunk.info <- chunks[train.chunk, ]
        chrom <- paste(chunk.info$chunkChrom)
        chunk.path <- file.path("chunks", train.chunk)
        counts.RData <- file.path(chunk.path, "counts.RData")
        load(counts.RData)
        regions.RData <- file.path(chunk.path, "regions.RData")
        load(regions.RData)
        regions.dt <- data.table(regions)
        counts.dt <- data.table(counts)
        counts.dt[, count := coverage]
        setkey(regions.dt, chromStart, chromEnd)
        setkey(counts.dt, chromStart, chromEnd)
        regions.dt$region.i <- 1:nrow(regions.dt)
        chrom <- paste(regions$chrom[1])
        max.chromEnd <- max(counts$chromEnd)
        min.chromStart <- min(counts$chromStart)

        train.peaks <- train.peaks.by.chunk[[train.chunk]]
        props.dt <- regions.dt[, list(
          prop.negative=mean(annotation=="noPeaks")
          ), by=.(chromStart, chromEnd)]
        props.dt[, reduce := (chromEnd - chromStart)/3 ]
        props.dt[, chromStart := as.integer(chromStart+reduce) ]
        props.dt[, chromEnd := as.integer(chromEnd-reduce) ]
        to.cluster <- rbind(
          props.dt[prop.negative==1, .(chromStart, chromEnd)],
          train.peaks[, c("chromStart", "chromEnd")])
        clustered <- clusterPeaks(to.cluster)
        problems <- clustered2problems(clustered)
        cat(sprintf(
          "%4d / %4d chunks %s problems=%d\n",
          train.chunk.i, length(train.peaks.by.chunk),
          train.chunk, nrow(problems)))
        
        problemsPlot <- 
          ggplot()+
            geom_segment(aes(problemStart/1e3, cluster,
                             xend=problemEnd/1e3, yend=cluster),
                         data=data.table(problems, sample.id="problems"))+
            scale_y_continuous("aligned read coverage",
                               breaks=function(limits){
                                 floor(limits[2])
                               })+
            xlab("position on chromosome (kilo bases = kb)")+
            scale_linetype_manual("error type",
                                  limits=c("correct", 
                                    "false negative",
                                    "false positive"
                                           ),
                                  values=c(correct=0,
                                    "false negative"=3,
                                    "false positive"=1))+
            geom_tallrect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
                              fill=annotation),
                          alpha=0.5,
                          color="grey",
                          data=regions)+
            scale_fill_manual(values=ann.colors)+
            theme_bw()+
            theme(panel.margin=grid::unit(0, "cm"))+
            facet_grid(sample.id ~ ., labeller=function(var, val){
              sub("McGill0", "", sub(" ", "\n", val))
            }, scales="free")+
            geom_step(aes(chromStart/1e3, coverage),
                      data=counts,
                      color="grey50")+
            geom_segment(aes(chromStart/1e3, 0,
                             xend=chromEnd/1e3, yend=0),
                         color="deepskyblue",
                         size=2,
                         data=train.peaks)
        ## print(problemsPlot)
        
        train.problems.by.problem <- foreach(problem.i=1:nrow(problems))%dopar%{
          problem <- problems[problem.i,]
          outside <- function(start, end){
            end < problem$problemStart |
              problem$problemEnd < start
          }
          problem.regions <- regions.dt[!outside(chromStart, chromEnd), ]
          if(0 < nrow(problem.regions)){
            problem.counts <- counts.dt[!outside(chromStart, chromEnd), ]
            problem.profile.list <- ProfileList(problem.counts)
            fit <- PeakSegJointSeveral(problem.profile.list)
            converted <- ConvertModelList(fit)
            if(is.data.frame(converted$peaks)){
              error.list <- PeakSegJointError(converted, problem.regions)
              min.error.models <-
                subset(error.list$modelSelection, errors==min(errors))
              simplest.model <- subset(min.error.models, peaks==min(peaks))
              simplest.peaks <- subset(
                converted$peaks, peaks==simplest.model$peaks)
              peaks.str <- paste(simplest.model$peaks)
              problem.error.regions <- error.list$error.regions[[peaks.str]]
              problemPlot <- 
                ggplot()+
                  scale_y_continuous("aligned read coverage",
                                     breaks=function(limits){
                                       floor(limits[2])
                                     })+
                  xlab("position on chromosome (kilo bases = kb)")+
                  scale_linetype_manual("error type",
                                        limits=c("correct", 
                                          "false negative",
                                          "false positive"
                                                 ),
                                        values=c(correct=0,
                                          "false negative"=3,
                                          "false positive"=1))+
                  geom_tallrect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
                                    fill=annotation),
                                alpha=0.5,
                                color="grey",
                                data=problem.regions)+
                  geom_tallrect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
                                    linetype=status),
                                color="black",
                                fill=NA,
                                size=1,
                                data=problem.error.regions)+
                  scale_fill_manual(values=ann.colors)+
                  theme_bw()+
                  theme(panel.margin=grid::unit(0, "cm"))+
                  facet_grid(sample.id ~ ., labeller=function(var, val){
                    sub("McGill0", "", sub(" ", "\n", val))
                  }, scales="free")+
                  geom_step(aes(chromStart/1e3, coverage),
                            data=problem.counts,
                            color="grey50")+
                  geom_segment(aes(chromStart/1e3, 0,
                                   xend=chromEnd/1e3, yend=0),
                               color="deepskyblue",
                               size=2,
                               data=simplest.peaks)
              list(
                features=featureMatrix(problem.profile.list),
                target=error.list$target)
            }#is.data.frame(peaks)
          }
        }#problem.i
        names(train.problems.by.problem) <- 
          problems[, sprintf("%s:%d-%d", chrom, problemStart, problemEnd)]
        PeakSegJoint.problems[[train.chunk]] <- train.problems.by.problem
      }#train.chunk
      RData.dir <- dirname(RData.file)
      dir.create(RData.dir, showWarnings=FALSE)
      save(PeakSegJoint.problems, file=RData.file)
    }#file.exists
  }#testSet.i
}#set

cat("DONE", file="PeakSegJoint.problems.txt")
