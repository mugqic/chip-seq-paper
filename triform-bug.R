works_with_R("3.2.0",
             triform="1.10.0")

params <-
  list(COVER.PATH="TF-benchmark",
       OUTPUT.PATH="TF-benchmark/nrsf_triform_MAX.P=0.050000.csv",
       CONTROLS=c("backgr_huds_k562_rep1.bed", "backgr_huds_k562_rep2.bed"),
       TARGETS = c("nrsf_huds_k562_rep1.bed", "nrsf_huds_k562_rep2.bed"),
       MAX.P = 0.05, MIN.WIDTH = 10, MIN.QUANT = 0.375, MIN.SHIFT = 10, 
       FLANK.DELTA = 150, CHRS = "chrY")
fit <- triform(configPath=NULL, params=params)
