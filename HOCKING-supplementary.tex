% -*- compile-command: "make HOCKING-supplementary.pdf" -*-
\documentclass{article}
\usepackage{natbib}
\usepackage{hyperref}
\usepackage{fullpage}
\usepackage{graphicx}
\newcommand{\RR}{\mathbb R}
\usepackage{amsmath,amssymb}
\newcommand{\annsquare}[1]{
  \textcolor{#1}{\rule{0.1in}{0.1in}}
}
\usepackage{color}
\usepackage{xcolor}
\definecolor{noPeaks}{HTML}{F6F4BF}
\definecolor{peakStart}{HTML}{FFAFAF}
\definecolor{peakEnd}{HTML}{FF4C4C}
\definecolor{peaks}{HTML}{A445EE}
\input{supplementary}

\begin{document}

\title{Figures and tables for\\``Visual annotations and a
  supervised learning approach for evaluating and calibrating ChIP-seq
  peak detectors''}

\author{Toby Dylan Hocking, Patricia Goerner-Potvin, Andreanne Morin,
  Xiaojian Shao,\\Tomi Pastinen and Guillaume Bourque}

\maketitle

\section*{Example of annotated region data file}

Below is an example of an annotation file that TDH created by manual
visual inspection of the H3K4me3 immune cell samples (tcell, bcell, and
monocyte). It is divided into 4 windows, each of which is separated by
two returns/newlines. Each line represents an annotated region for
several cell types. The cell types that are not listed all get a
noPeaks annotation in the indicated region. Genomic regions were
copied from the UCSC genome browser web page, and pasted into a text
file.

\begin{verbatim}
chr11:118,092,641-118,095,026 peakStart monocyte tcell
chr11:118,095,334-118,096,640 peakEnd monocyte tcell
chr11:118,101,452-118,118,472 peaks
chr11:118,121,649-118,124,175 peaks monocyte tcell bcell

chr11:111,285,081-111,285,355 peakStart monocyte
chr11:111,285,387-111,285,628 peakEnd monocyte
chr11:111,299,681-111,337,593 peaks
chr11:111,635,157-111,636,484 peakStart monocyte tcell bcell
chr11:111,637,473-111,638,581 peakEnd monocyte tcell bcell

chr1:32,717,194-32,721,976 peaks tcell
chr1:32,750,608-32,756,699 peaks
chr1:32,757,261-32,758,801 peaks tcell bcell monocyte

chr2:26,567,544-26,568,406 peakStart bcell tcell monocyte
chr2:26,568,616-26,568,862 peakEnd bcell tcell monocyte
chr2:26,569,573-26,571,905 peakEnd bcell tcell monocyte
chr2:26,578,595-26,632,223 peaks
chr2:26,634,282-26,636,118 peaks monocyte
\end{verbatim}

\newpage \section*{Definition of the annotation error}

In this section we give the precise mathematical definition of the
annotation error.

\subsection*{Data definition}

Let there be $n$ annotated training samples, all of the same histone
mark type. For simplicity, and without loss of generality, let us
consider just one chromosome with $d$ base pairs. Let $\mathbf
x_1\in\RR^d, \dots, \mathbf x_n\in\RR^d$ be the vectors of coverage
across that chromosome. For example $d=249,250,621$ is the number of
base pairs on chr1, and $\mathbf x_i\in\RR^d$ is the H3K4me3 coverage
profile on chr1 for one sample $i\in\{1, \dots, n\}$.

We also have exactly 4 sets of annotated regions $\underline R_i,
\overline R_i, R_i^+, R_i^-)$ for each sample $i\in\{1, \dots, n\}$:
\begin{center}
  \begin{tabular}{rccc}
  Data & Type & Color &Symbols \\
  \hline
  Coverage& &  & $\mathbf x_1, \dots \mathbf x_n$ \\
  Weak annotations & peaks
  & \annsquare{peaks}
  & $R_1^+, \dots, R_n^+$ \\
  & noPeaks 
  & \annsquare{noPeaks}
  & $R_1^-, \dots, R_n^-$ \\
  Strong annotations 
  & peakStart 
  & \annsquare{peakStart}
  & $\underline R_1, \dots, \underline R_n$ \\
  & peakEnd 
  & \annsquare{peakEnd}
  & $\overline R_1, \dots, \overline R_n$ \\
 \end{tabular}
\end{center}
For each sample $i\in\{1, \dots, n\}$, $R_i^+$ is a set of regions,
and each region is an interval of base pairs. 

A peak detection function or peak caller $c:\RR^d \rightarrow \{0, 1\}^d$ 
gives a binary peak call prediction $\mathbf y=c(\mathbf x)\in\{0,
1\}^d$ given some coverage profile $\mathbf x$.

The goal is to learn how to call peaks $c(\mathbf x_i)$ which agree
with the annotated regions $R_i^+, R_i^-, \underline R_i, \overline
R_i$ for some test samples $i$. To quantify the error of the peak
calls with respect to the annotation data, we define the following
functions.

\subsection*{Weak annotation error}

The weak annotations $R_i^+,R_i^-$ count whether there are any peaks
overlapping a region. They are called weak because each peaks region
can only produce a false negative (but not a false positive), and each
noPeaks region can only produce a false positive (but not a false
negative). Let
\begin{equation}
  B(\mathbf y, \mathbf r) = \sum_{j\in \mathbf r} y_j
\end{equation}
be the number of bases which have peaks overlapping region $\mathbf
r$. Then for a sample $i$, the number of weak false positives is
\begin{equation}
  \label{eq:WFP}
  \text{WFP}(\mathbf y, R_i^-) = 
  \sum_{\mathbf r \in R_i^-} 
  I\left[ B(\mathbf y, \mathbf r) > 0 \right]
\end{equation}
and the number of weak true positives is
\begin{equation}
  \label{eq:WTP}
  \text{WTP}(\mathbf y, R_i^+) = 
  \sum_{\mathbf r \in R_i^+} 
  I\left[ B(\mathbf y, \mathbf r) > 0 \right],
\end{equation}
where $I$ is the indicator function.

\subsection*{Strong annotation error}

The strong annotations $\underline R_i, \overline R_i$ count the
number of peak starts and ends occuring in the given regions. They are
called strong because each region can produce a false positive (more
than 1 peak start/end predicted in the region) or a false negative (no
peak start/end predicted). First, let $y_0 = y_{d+1} = 0$ and define
the set of first bases of all peaks as
\begin{equation}
  \label{eq:underI}
  \underline {\mathcal I}(\mathbf y) = \left\{
  j\in\{1, \dots, d\}: y_j=1 \text{ and } y_{j-1}=0
  \right\}
\end{equation}
and the set of last bases of all peaks as
\begin{equation}
  \label{eq:overI}
  \overline {\mathcal I}(\mathbf y) = \left\{
  j\in\{1, \dots, d\}: y_{j}=1\text{ and } y_{j+1}=0
  \right\}.
\end{equation}

For a sample $i$, the number of strong false positives is
\begin{equation}
  \label{eq:SFP}
  \text{SFP}(\mathbf y, \underline R_i, \overline R_i) =
  \sum_{\mathbf r\in\underline R_i}
  I\big[ |\mathbf r\cap \underline {\mathcal I}(\mathbf y)| > 1 \big] +
  \sum_{\mathbf r\in\overline R_i}
  I\big[ |\mathbf r\cap \overline {\mathcal I}(\mathbf y)| > 1 \big], 
\end{equation}
and the number of strong true positives is
\begin{equation}
  \label{eq:STP}
  \text{STP}(\mathbf y, \underline R_i, \overline R_i) =
  \sum_{\mathbf r\in\underline R_i}
  I\big[ |\mathbf r\cap \underline {\mathcal I}(\mathbf y)| > 0 \big] +
  \sum_{\mathbf r\in\overline R_i}
  I\big[ |\mathbf r\cap \overline {\mathcal I}(\mathbf y)| > 0 \big].
\end{equation}

\subsection*{Total annotation error}

For a sample $i$, the total number of false positives is
\begin{equation}
  \label{eq:FP}
  \text{FP}(\mathbf y,  \underline R_i, \overline R_i, R_i^-) =
  \text{WFP}(\mathbf y, R_i^-)+
  \text{SFP}(\mathbf y, \underline R_i, \overline R_i),
\end{equation}
the total number of true positives is
\begin{equation}
  \label{eq:FN}
  \text{TP}(\mathbf y, \underline R_i, \overline R_i, R_i^+) =
  \text{WTP}(\mathbf y, R_i^+) +
  \text{STP}(\mathbf y, \underline R_i, \overline R_i),
\end{equation}
the total number of false negatives is
\begin{equation}
  \label{eq:FN}
  \text{FN}(\mathbf y, \underline R_i, \overline R_i, R_i^+) =
  |\underline R_i| + |\overline R_i| + |R_i^+|
  -\text{TP}(\mathbf y, \underline R_i, \overline R_i, R_i^+).
\end{equation}
The annotation error $E$
quantifies the number of incorrect annotated regions:
\begin{equation}
  \label{eq:E}
  E(\mathbf y,  \underline R_i, \overline R_i, R_i^+, R_i^-) =
  \text{FP}(\mathbf y,  \underline R_i, \overline R_i, R_i^-) +
  \text{FN}(\mathbf y, \underline R_i, \overline R_i, R_i^+).
\end{equation}
The annotation error $E$ can be easily computed using the C code
in the R package PeakError on GitHub:
\url{https://github.com/tdhock/PeakError}.

\subsection*{ROC analysis}

A standard unsupervised peak caller can be characterized as a function
$c_\lambda:\RR^d \rightarrow \{0, 1\}^d$, where the significance
threshold $\lambda\in\RR$ controls the number of peaks detected. 
Receiver Operating Characteristic (ROC) curves can also be used to
show the train error of all thresholds of a peak detector on an
annotation data set. Define the false positive rate as
\begin{equation}
  \label{eq:FPR}
  \text{FPR}(\lambda) =
  \frac{
    \sum_{i=1}^n
    \text{FP}\left[
      c_\lambda(\mathbf x_i),  \underline R_i, \overline R_i, R_i^-
    \right]
  }{
   |\underline R_i| + |\overline R_i| + |R_i^-| 
 }
\end{equation}
and the true positive rate as
\begin{equation}
  \label{eq:TPR}
  \text{TPR}(\lambda) =
  \frac{
    \sum_{i=1}^n
    \text{TP}\left[ c_\lambda(\mathbf x_i), \underline R_i, \overline R_i, R_i^+
    \right]
  }{
    |\underline R_i| + |\overline R_i| + |R_i^+|
  }.
\end{equation}

ROC curves are traced by plotting $\text{TPR}(\lambda)$ versus
$\text{FPR}(\lambda)$ for all possible values of the peak detection
threshold $\lambda$.


\newpage \section*{Details of peak calling algorithms}

\citet{MACS} proposed Model-based Analysis of ChIP-Seq (MACS). We
downloaded MACS version 2.0.10 12162013 (commit
ca806538118a85ec338674627f0ac53ea17877d9 on GitHub). The significance
threshold $\lambda$ is the qvalue cutoff parameter \texttt{-q}. We
used a grid of 59 qvalue cutoffs from 0.8 to $10^{-15}$. 

The macs broad peak caller is more difficult to train because it has 2
different qvalue cutoff parameters (\texttt{-q} and
\texttt{--broad-cutoff}). So we used the same grid of 59 qvalue
cutoffs for \texttt{-q}, and then defined
\begin{equation}
  \label{eq:broad-cutoff}
  \texttt{broad-cutoff} = \texttt{q} \times 1.122.
\end{equation}

% If more computation time is available then it would be preferable to
% search a 2-dimensional grid of \texttt{--broad-cutoff} and \texttt{-q}
% parameters. However for this study we limited our search to 1
% parameter per algorithm. 

The default macs algorithms use a default
qvalue cutoff of $\tilde \lambda = 0.05$.

% \citet{CCAT} proposed the CCAT algorithm. We downloaded CCAT version
% 3.0 from \url{http://cmb.gis.a-star.edu.sg/ChIPSeq/paperCCAT.htm}. We
% defined the significance threshold $\lambda$ as the minScore
% parameter. The default CCAT algorithm uses a minScore of $\tilde
% \lambda = 5$.

\citet{HMCan} proposed the HMCan algorithm which uses GC-content and
copy number normalization. We downloaded HMCan commit
9d0a330d0a873a32b9c4fa72c94d00968132b9ef from BitBucket. We used the
default GC content normalization file provided by the authors. We used
two different parameter files to test two different peak detectors: 
\begin{center}
  \begin{tabular}{cr}
  name & mergeDistance \\
  \hline
  hmcan & 200 \\
  hmcan.broad & 1000
\end{tabular}
\end{center}
We then ran HMCan with finalThreshold=0, and
defined $\lambda$ as a threshold on column 5 in the
regions.bed file. Both hmcan and hmcan.broad use a default finalThreshold of
$\tilde \lambda = 10$.

\citet{RSEG} proposed the RSEG algorithm. We downloaded RSEG version
0.4.8 from \url{http://smithlabresearch.org/software/rseg/}. Upon
recommendation of the authors, we saved computation time by running
\texttt{rseg-diff} using options \texttt{-training-size 100000} and
\texttt{-i 20}. We used the \texttt{-d} option to specify a dead
regions file for hg19 based on our alignment pipeline. We defined the
significance threshold $\lambda$ as the sum of posterior scores
(column 6 in the output .bed file). For the default RSEG algorithm, we
used all the peaks in the output file, meaning a posterior score
threshold of $\tilde \lambda = 0$.

\citet{SICER} proposed the SICER algorithm. We downloaded SICER
version 1.1 from \url{http://home.gwu.edu/~wpeng/SICER\_V1.1.tgz}. We
defined the significance threshold $\lambda$ as the FDR (column 8) in
the islands-summary output file. For the default SICER algorithm, we
used an FDR of $\tilde \lambda=0.01$ as suggested in the README and
example.

\citet{homer} proposed the HOMER set of tools for DNA motif
detection. We used the \texttt{findPeaks} program in HOMER version 4.1
with the \texttt{-style histone} option. We defined the significance
threshold $\lambda$ as the ``p-value vs Control'' column. We defined
the default model as all peaks in the output file.


\newpage \section*{\figtrain: annotation error curves for all
  algorithms and data sets}

\begin{center}
  \includegraphics[width=\textwidth]{figure-train-error-all}
\end{center}
Train error curves for each data set (rows) and each algorithm
(columns). A vertical red line shows the default parameter for each
model, and a horizontal grey line shows the minimum error model for
each data set.

\newpage \section*{\figroc: ROC curves for train error of all
  algorithms on all data sets}

\begin{center}
  \includegraphics[width=\textwidth]{figure-roc-H3K4me3}
\end{center}

It is clear that macs is the best for H3K4me3 data.

\begin{center}
  \includegraphics[width=\textwidth]{figure-roc-H3K36me3}
\end{center}

It is clear that hmcan.broad is the best for H3K36me3 data.

\newpage \section*{\figlearned: examples of fitted peak detectors}

\begin{center}
  \includegraphics[width=\textwidth]{figure-learned-1}
\end{center}

\begin{center}
  \includegraphics[width=\textwidth]{figure-learned-2}
\end{center}

Shown are default and trained algorithms for samples of 2 different
histone mark types. Consistent with the quantitative results, it is
clear that macs.trained is the best for the H3K4me3 data, and that
hmcan.broad.trained is the best for the H3K36me3 data.
 
\newpage \section*{\figseveral: Windows labeled by 2 different
  annotators}

Annotated regions for 2 different annotators (top=AM, bottom=TDH) on
the same genomic regions and sample sets (H3K36me3 immune cell
types). It is clear that the annotators focus on different levels of
detail, but have in general the same visual definition of a peak.

\begin{center}
  \includegraphics[width=\textwidth]{figure-several-annotators}
\end{center}

% We also show all profiles, for several annotated windows:

% \begin{center}
%   \includegraphics[width=\textwidth]{figure-several-annotators-1}
% \end{center}

% \begin{center}
%   \includegraphics[width=\textwidth]{figure-several-annotators-2}
% \end{center}

% \begin{center}
%   \includegraphics[width=\textwidth]{figure-several-annotators-3}
% \end{center}

% \begin{center}
%   \includegraphics[width=\textwidth]{figure-several-annotators-4}
% \end{center}

% \begin{center}
%   \includegraphics[width=\textwidth]{figure-several-annotators-5}
% \end{center}

% \begin{center}
%   \includegraphics[width=\textwidth]{figure-several-annotators-6}
% \end{center}

% \begin{center}
%   \includegraphics[width=\textwidth]{figure-several-annotators-7}
% \end{center}

\newpage \section*{\figannotators: train on one annotator, test on
  another annotator}

\begin{center}
  \includegraphics[width=\textwidth]{figure-test-annotators}
\end{center}
H3K4me3 immune cell data sets were annotated by TDH and PGP. We
trained models on one or the other annotator, and then tested those
models on the same or a different annotator.
We repeated 100 trials of the following computational experiment: we
randomly designated half of the annotated windows as test data in each
data set, then selected the parameter $\hat \lambda$ with minimum
annotation error on a train set of 10 annotated windows. 

It is clear
for all models that it does not make much difference in terms of test
error when training on one or another annotator.

\newpage
 \section*{\figtypes: train on some cell types, test on
 some other cell types}

\begin{center}
  \includegraphics[width=\textwidth]{figure-test-types}
\end{center}
TDH annotated H3K4me3 data sets of immune and other cell types. We
trained models on one or the other cell types, and then tested those
models on the same or a different set of cell types.  We repeated 100
trials of the following computational experiment: we randomly
designated half of the annotated windows as test data in each data
set, then selected the parameter $\hat \lambda$ with minimum
annotation error on a train set of 10 annotated windows.

It is clear that for some models such as macs and macs.broad, a model
trained on the different cell types (e.g. right panel: train on
immune, test on other) yields higher test error than a model trained
on the same cell types (e.g. train on other, test on other). It is
also clear that the train data do not make much difference for other
models, such as rseg, sicer, hmcan, hmcan.broad, and homer.

\bibliographystyle{abbrvnat}
\bibliography{refs}

\end{document}
