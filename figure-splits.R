source("packages.R")
load("chunks.RData")
load("dp.peaks.sets.RData")

hg19 <- fread("~/chromInfo.txt")
setnames(hg19, c("chrom", "bases", "file"))
setkey(hg19, chrom)
chrom.order <- paste0("chr", c(1:22, c("X", "Y")))
chrom.dt <- hg19[chrom.order]
chrom.dt[, chrom.fac := factor(chrom, chrom.order)]

chunk.dt <- data.table(chunks)
chunk.dt[, regionMid := as.integer((regionStart + regionEnd)/2)]
chunk.dt[, chrom.fac := factor(chunkChrom, chrom.order)]
chunk.dt[, chunk.name := paste0(set.name, "/", chunk.id)]
setkey(chunk.dt, chunk.name)

u.prefix <- "http://cbio.ensmp.fr/~thocking/chip-seq-chunk-db"
u.prefix <- ".."
tf.prefix <- "http://cbio.ensmp.fr/~thocking/table-TF-benchmark"

gg.chunks <- 
ggplot()+
  geom_segment(aes(bases/1e6, chrom.fac,
                   xend=0, yend=chrom.fac),
               color="grey50",
               data=chrom.dt)+
  geom_point(aes(
    regionMid/1e6, chrom.fac,
    href=ifelse(
      grepl("NTNU", set.name),
      sprintf("%s/%s_%s_total.png", tf.prefix, experiment, chunk.id),
      sprintf("%s/%s/%s/regions.png", u.prefix, set.name, chunk.id))),
             shape=1,
             data=chunk.dt)+
  theme_bw()+
  theme(panel.margin=grid::unit(0, "lines"))+
  theme_animint(width=1500)+
  facet_grid(. ~ set.name, labeller=function(var, val){
    gsub("_", "\n", val)
  })+
  ylab("chromosome")+
  scale_x_continuous("position on chromosome (Mb = mega bases)",
                     breaks=c(0, 100, 200))

pdf("figure-splits-chunks.pdf", 10, 4)
print(gg.chunks)
dev.off()

viz <- list(
  title="Labeled genomic windows in 10 benchmark data sets",
  chunks=gg.chunks)
animint2dir(viz, "chunks/figure-splits-chunks")

## Train on one set, test on same set.
set.name.vec <- c("H3K4me3_TDH_other", "H3K36me3_TDH_other")
for(set.name in set.name.vec){
  chunks.by.fold <- dp.peaks.sets[[set.name]]
  train.chunks <- do.call(c, chunks.by.fold[-3])
  test.chunks <- chunks.by.fold[[3]]
  chunk.dt[, set := "unused"]
  chunk.dt[test.chunks, set := "test"]
  chunk.dt[train.chunks, set := "train"]
  gg.chunks <- 
    ggplot()+
      geom_segment(aes(bases/1e6, chrom.fac,
                       xend=0, yend=chrom.fac),
                   color="grey50",
                   data=chrom.dt)+
      geom_point(aes(regionMid/1e6, chrom.fac,
                     color=set,
                     href=sprintf(
                       "%s/%s/%s/regions.png", u.prefix, set.name, chunk.id)
                     ),
                 shape=1,
                 data=chunk.dt)+
      scale_color_manual(values=c(unused="grey50",
                           test="blue",
                           train="red"))+
      theme_bw()+
      theme(panel.margin=grid::unit(0, "lines"))+
      theme_animint(width=1500)+
      facet_grid(. ~ set.name, labeller=function(var, val){
        gsub("_", "\n", val)
      })+
      ylab("chromosome")+
      scale_x_continuous("position on chromosome (Mb = mega bases)",
                         breaks=c(0, 100, 200))
  pdf(sprintf("figure-splits-chunks-%s.pdf", set.name), 10, 4)
  print(gg.chunks)
  dev.off()
}

## Train on one set, test on another set.
tt.sets <-
  list(H3K36me3.types=c("H3K36me3_TDH_immune", "H3K36me3_TDH_other"),
       H3K4me3.types=c("H3K4me3_TDH_immune", "H3K4me3_TDH_other"),
       H3K36me3.annotators=c("H3K36me3_TDH_immune", "H3K36me3_AM_immune"),
       H3K4me3.annotators=c("H3K4me3_TDH_immune", "H3K4me3_PGP_immune"),
       TDH.experiments=c("H3K36me3_TDH_immune", "H3K4me3_TDH_immune"))
test.fold <- 1
for(split.dot in names(tt.sets)){
  split.name <- sub("[.]", "-", split.dot)
  tt.set <- tt.sets[[split.dot]]
  test.set <- tt.set[1]
  chunks.all.sets.list <- list()
  for(train.set in tt.set){
    train.chunks <- do.call(c, dp.peaks.sets[[train.set]][-test.fold])
    test.chunks <- dp.peaks.sets[[test.set]][[test.fold]]
    setkey(chunk.dt, chunk.name)
    chunk.dt[, set := "unused"]
    chunk.dt[test.chunks, set := "test"]
    chunk.dt[train.chunks, set := "train"]
    chunks.all.sets.list[[train.set]] <-
      data.table(train.set, chunk.dt)
  }
  chunks.all.sets <- do.call(rbind, chunks.all.sets.list)
  gg.chunks <- 
    ggplot()+
      geom_segment(aes(bases/1e6, chrom.fac,
                       xend=0, yend=chrom.fac),
                   color="grey50",
                   data=chrom.dt)+
      geom_point(aes(regionMid/1e6, chrom.fac,
                     color=set,
                     href=sprintf(
                       "%s/%s/%s/regions.png", u.prefix, set.name, chunk.id)
                     ),
                 shape=1,
                 data=chunks.all.sets)+
      scale_color_manual(values=c(unused="grey50",
                           test="blue",
                           train="red"))+
      theme_bw()+
      theme(panel.margin=grid::unit(0, "lines"))+
      theme_animint(width=1500)+
      facet_grid(train.set ~ set.name, labeller=function(var, val){
        if(var=="set.name"){
          gsub("_", "\n", val)
        }else{
          paste("train ", val)
        }
      })+
      ylab("chromosome")+
      scale_x_continuous("position on chromosome (Mb = mega bases)",
                         breaks=c(0, 100, 200))
  pdf(sprintf("figure-splits-%s.pdf", split.name), 10, 7)
  print(gg.chunks)
  dev.off()
}

## Train on one set, test on same set, several size train sets.
set.name <- "H3K36me3_AM_immune"
test.fold <- 1
chunks.all.sizes.list <- list()
for(train.set.size in c(2, 5, 10)){
  chunks.by.fold <- dp.peaks.sets[[set.name]]
  train.chunks <- do.call(c, chunks.by.fold[-test.fold])[1:train.set.size]
  test.chunks <- chunks.by.fold[[test.fold]]
  setkey(chunk.dt, chunk.name)
  chunk.dt[, set := "unused"]
  chunk.dt[test.chunks, set := "test"]
  chunk.dt[train.chunks, set := "train"]
  setkey(chunk.dt, set.name)
  chunks.all.sizes.list[[paste(train.set.size)]] <-
    data.table(train.set.size, chunk.dt[set.name])
}
chunks.all.sizes <- do.call(rbind, chunks.all.sizes.list)
gg.chunks <- 
  ggplot()+
    geom_segment(aes(bases/1e6, chrom.fac,
                     xend=0, yend=chrom.fac),
                 color="grey50",
                 data=chrom.dt)+
    geom_point(aes(regionMid/1e6, chrom.fac,
                   color=set,
                   href=sprintf(
                     "%s/%s/%s/regions.png", u.prefix, set.name, chunk.id)
                   ),
               shape=1,
               data=chunks.all.sizes)+
    scale_color_manual(values=c(unused="grey50",
                         test="blue",
                         train="red"))+
    theme_bw()+
    theme(panel.margin=grid::unit(0, "lines"))+
    theme_animint(width=1500)+
    facet_grid(set.name ~ train.set.size, labeller=function(var, val){
      if(var=="set.name"){
        paste(val)
      }else{
        paste(val, "labeled windows")
      }
    })+
    ylab("chromosome")+
    scale_x_continuous("position on chromosome (Mb = mega bases)",
                       breaks=c(0, 100, 200))
pdf("figure-splits-sizes.pdf", 7, 4)
print(gg.chunks)
dev.off()

## Train on one set, test on same set, show all 4 test folds.
set.name <- "H3K36me3_TDH_other"
chunks.all.folds.list <- list()
for(test.fold in 1:4){
  chunks.by.fold <- dp.peaks.sets[[set.name]]
  train.chunks <- do.call(c, chunks.by.fold[-test.fold])
  test.chunks <- chunks.by.fold[[test.fold]]
  setkey(chunk.dt, chunk.name)
  chunk.dt[, set := "unused"]
  chunk.dt[test.chunks, set := "test"]
  chunk.dt[train.chunks, set := "train"]
  setkey(chunk.dt, set.name)
  chunks.all.folds.list[[test.fold]] <-
    data.table(test.fold, chunk.dt[set.name])
}
chunks.all.folds <- do.call(rbind, chunks.all.folds.list)

gg.chunks <- 
  ggplot()+
    geom_segment(aes(bases/1e6, chrom.fac,
                     xend=0, yend=chrom.fac),
                 color="grey50",
                 data=chrom.dt)+
    geom_point(aes(regionMid/1e6, chrom.fac,
                   color=set,
                   href=sprintf(
                     "%s/%s/%s/regions.png", u.prefix, set.name, chunk.id)
                   ),
               shape=1,
               data=chunks.all.folds)+
    scale_color_manual(values=c(unused="grey50",
                         test="blue",
                         train="red"))+
    theme_bw()+
    theme(panel.margin=grid::unit(0, "lines"))+
    theme_animint(width=1500)+
    facet_grid(set.name ~ test.fold, labeller=function(var, val){
      if(var=="set.name"){
        paste(val)
      }else{
        paste("test fold", val)
      }
    })+
    ylab("chromosome")+
    scale_x_continuous("position on chromosome (Mb = mega bases)",
                       breaks=c(0, 100, 200))
pdf("figure-splits.pdf", 7, 4)
print(gg.chunks)
dev.off()

