source("packages.R")

load("dp.peaks.regression.RData")

err.dt <- data.table(dp.peaks.regression$error)
peak.wide <- dcast.data.table(err.dt, set.name ~ peaks)
peak.wide.mat <- as.matrix(peak.wide[,-1, with=FALSE])
peak.wide$mean <- apply(peak.wide.mat, 1, function(count.vec)sum(count.vec * (0:9))/sum(count.vec))
count.dt <- err.dt[, list(
  samples=length(unique(sample.id)),
  windows=length(unique(test.chunk))
  ), by=set.name]
setkey(count.dt, set.name)
setkey(peak.wide, set.name)
out.dt <- peak.wide[count.dt]
names(out.dt)[1] <- "Peaks:"
check.mat <- rbind(
  rowSums(peak.wide.mat),
  with(out.dt, samples * windows))
stopifnot(check.mat[1,] == check.mat[2,])

peak.dt <- data.table(
  set.name=sub("/.*", "", names(peak.vec)),
  chunk.name=names(peak.vec),
  min=peak.mat[1,],
  mean=peak.mat[2,],
  max=peak.mat[3,])

peak.dt[, list(
  min=mean(min),
  mean=mean(mean),
  max=mean(max),
  windows=.N
  ), by=.(set.name)]

xt <- xtable(out.dt, align="rrrrrrrrrrrrrrr")
print(xt, file="table-pred-peak-counts.tex", floating=FALSE, include.rownames=FALSE)
