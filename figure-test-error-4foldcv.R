if(!file.exists("4foldcv-predicted-peaks.csv")){
  download.file("http://members.cbio.mines-paristech.fr/~thocking/chip-seq-chunk-db/4foldcv-predicted-peaks.csv", "4foldcv-predicted-peaks.csv")
}
library(data.table)
pred.peaks <- fread("4foldcv-predicted-peaks.csv")
if(!file.exists("4foldcv-test-folds.csv")){
  download.file("http://members.cbio.mines-paristech.fr/~thocking/chip-seq-chunk-db/4foldcv-test-folds.csv", "4foldcv-test-folds.csv")
}
test.folds <- fread("4foldcv-test-folds.csv")

algorithms <- pred.peaks[, list(peaks=.N), by=list(algorithm)]

pred.error <- pred.peaks[, {
  regions.RData <- paste0("chunks/", test.chunk, "/regions.RData")
  if(!file.exists(regions.RData)){
    dir.create(dirname(regions.RData), recursive=TRUE)
  }
  chunk.peaks <- data.table(.SD)
  setkey(chunk.peaks, sample.id, algorithm)
  load(regions.RData)
  data.table(regions)[, {
    sample.regions <- .SD
    algorithms[, {
      i <- data.table(sample.id, algorithm)
      algo.peaks <- chunk.peaks[i, nomatch=0L]
      error.regions <- PeakErrorChrom(algo.peaks, sample.regions)
      data.table(error.regions)[, list(
        errors=sum(fp+fn),
        labels=.N)]
    }, by=list(algorithm)]
  }, by=list(sample.id=paste(sample.id))]
}, by=list(test.chunk)]

fold.totals <- pred.error[test.folds, on=list(test.chunk)][, list(
  errors=sum(errors),
  labels=sum(labels)
  ), by=list(set.name=sub("/.*", "", test.chunk), fold.id, algorithm)]
fwrite(fold.totals, "4foldcv-test-error.csv")
fold.totals[, error.percent := 100*errors/labels]
algo.means <- fold.totals[, list(
  mean=mean(error.percent)
  ), by=list(algorithm)]
setkey(algo.means, mean)
fold.totals[, algo.fac := factor(algorithm, algo.means$algorithm)]
p <- lattice::dotplot(algo.fac ~ error.percent|set.name, fold.totals)
print(p)
pdf("figure-test-error-4foldcv.pdf")
print(p)
dev.off()
