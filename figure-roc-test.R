source("packages.R")
source("algo.colors.R")
source("pdf.png.R")
objs <- load("roc.test.RData")
load("dp.peaks.sets.RData")

sorted.models <- selected[, {
  data.table(mean.percent=mean(test.error.percent))
}, by=model][order(mean.percent),]
selected[, Model := factor(model, rev(sorted.models$model))]

median.dots <- selected[, {
  data.table(median.percent=median(test.error.percent))
}, by=.(set.name, Model)]

parameter.points[, Model := factor(model, rev(sorted.models$model))]

gg.test.error <- 
ggplot()+
  ## geom_point(aes(median.percent, Model, color=Model),
  ##            shape=20,
  ##            size=5,
  ##            alpha=0.5,
  ##            data=median.dots)+
  ## geom_point(aes(test.error.percent, Model, color=Model),
  ##            shape=1,
  ##            data=selected)+
  geom_point(aes(test.error.percent, Model, color=Model, shape=parameter),
             size=4,
             data=parameter.points[model!="PeakSegJoint",])+
  scale_shape_manual(values=c(default=20, learned=1))+
  scale_color_manual(values=algo.colors)+
  guides(color="none")+
  scale_x_continuous(
    paste("Test error",
          "(percent incorrect labels),",
          "4 circles = 4 cross validation folds"),
    breaks=seq(0, 80, by=20))+
  facet_grid(. ~ set.name, labeller=function(var, val){
    if(var=="set.i"){
      paste("test fold", val)
    }else{
      gsub("_", "\n", val)
    }
  })+
  theme_bw()+
  theme(panel.margin=grid::unit(0, "cm"))

pdf.png(gg.test.error, "figure-test-error-dots-all.pdf", width=13, height=3)
  
setkey(parameter.points, parameter, set.name, set.i, model)
points.wide <- parameter.points["default"]
points.learned <- parameter.points["learned"]
setkey(points.wide, set.name, set.i, model)
setkey(points.learned, set.name, set.i, model)
points.wide$learned.percent <- points.learned[points.wide, test.error.percent]

equality.df <- data.frame(intercept=0, slope=1)
gg.learned.default <-
  ggplot()+
  ggtitle(paste("learned model is more accurate than default model,",
                "except sometimes with small train set (H3K36me3_TDH_*)"))+
  scale_color_manual(values=algo.colors)+
  geom_abline(aes(slope=slope, intercept=intercept),
              color="grey",
              data=equality.df)+
  geom_point(aes(test.error.percent, learned.percent, color=Model),
             shape=2,
             data=points.wide)+
  facet_grid(. ~ set.name, labeller=function(var, val){
    if(var=="set.i"){
      paste("test fold", val)
    }else{
      paste(val)
      gsub("_", "\n", val)
    }
  })+
  coord_equal()+
  theme_bw()+
  scale_x_continuous(paste("test error of default model",
                           "(percent incorrect labels)",
                           sep=" "),
                     breaks=seq(0, 80, by=20))+
  scale_y_continuous(paste("test error of trained model",
                           "(percent incorrect labels)",
                           sep="\n"),
                     breaks=seq(0, 100, by=20))+
  theme(panel.margin=grid::unit(0, "cm"))

pdf("figure-test-learned-default.pdf", width=13, height=3)
print(gg.learned.default)
dev.off()

pdf("figure-test-learned-default-zoom.pdf", width=13, height=3)
print(gg.learned.default+coord_equal(xlim=c(-5, 45), ylim=c(-5, 45)))
dev.off()

trained.dt <- roc[!grepl("default", algorithm), ]
zero.dt <- trained.dt[, {
  if(FPR[1] < FPR[length(FPR)]){
    data.table(param.num=c(NA, param.num),
               TPR=c(0, TPR),
               FPR=c(0, FPR))
  }else{
    data.table(param.num=c(param.num, NA),
               TPR=c(TPR, 0),
               FPR=c(FPR, 0))
  }
}, by=.(set.name, set.i, model)]

some <- function(dt){
  dt[model!="PeakSegJoint",]
}
gg.roc.all <- ggplot()+
  geom_path(aes(FPR, TPR, group=model, color=model),
            data=some(zero.dt))+
  scale_color_manual(values=algo.colors)+
  scale_fill_manual(values=algo.colors)+
  ## geom_point(aes(FPR, TPR, fill=model, shape=parameter),
  ##            color="black",
  ##            data=parameter.points)+
  ## scale_shape_manual(values=c(default=22, learned=23))+
  geom_point(aes(FPR, TPR, color=model, shape=parameter),
             size=4,
             data=some(parameter.points))+
  scale_shape_manual(values=c(default=20, learned=1))+
  facet_grid(set.i ~ set.name, labeller=function(var, val){
    if(var=="set.i"){
      paste("test fold", val)
    }else{
      paste(val)
      gsub("_", "\n", val)
    }
  })+
  guides(fill=guide_legend(
           override.aes=list(shape=22, color="black")))+
  coord_equal(xlim=c(-0.05, 0.45))+
  theme_bw()+
  scale_x_continuous(paste(
    "False positive rate in test labels",
    "= Probability(peak | no peak)"),
                     breaks=seq(0, 0.4, by=0.2),
                     labels=c("0", "0.2", "0.4"))+
  scale_y_continuous(paste(
    "True positive rate in test labels",
    "= Probability(peak | peak)"),
                     breaks=seq(0, 1, by=0.2))+
  theme(panel.margin=grid::unit(0, "cm"))

##png("figure-roc-test.png", width=19, height=15, units="in", res=200) 
pdf.png(gg.roc.all, "figure-roc-test-all.pdf", width=12, height=10)

## plot with only 1 panel.
show.sets <- c("H3K36me3_TDH_other")
some <- function(dt){
  dt[set.name %in% show.sets,]
}
gg.test.some <- 
ggplot()+
  geom_point(aes(test.error.percent, Model, color=Model, shape=parameter),
             size=4,
             data=some(parameter.points))+
  scale_shape_manual(values=c(default=20, learned=1))+
  scale_color_manual(values=algo.colors)+
  guides(color="none")+
  scale_x_continuous(
    paste("Test error",
          "(percent incorrect labels),",
          "4 circles = 4 cross validation folds"),
    breaks=seq(0, 80, by=20))+
  facet_grid(. ~ set.name)+
  theme_bw()+
  theme(panel.margin=grid::unit(0, "cm"))
pdf.png(gg.test.some, "figure-test-error-dots-one.pdf", width=6, height=2.5)

zero.dt[, Model := factor(model, sorted.models$model)]
one <- function(dt){
  dt[set.name %in% show.sets & set.i==3,]
}
gg.roc.one <- ggplot()+
  geom_path(aes(FPR, TPR, group=Model, color=Model),
            data=one(zero.dt))+
  scale_color_manual(values=algo.colors)+
  scale_fill_manual(values=algo.colors)+
  geom_point(aes(FPR, TPR, color=Model, shape=parameter),
             size=4,
             data=one(parameter.points))+
  scale_shape_manual(values=c(default=20, learned=1))+
  facet_grid(. ~ set.name)+
  guides(fill=guide_legend(
           override.aes=list(shape=22, color="black")))+
  coord_equal(xlim=c(-0.05, 0.75))+
  theme_bw()+
  scale_x_continuous(paste(
    "False positive rate in test labels",
    "= Probability(peak | no peak)"),
                     breaks=seq(0, 0.4, by=0.2),
                     labels=c("0", "0.2", "0.4"))+
  scale_y_continuous(paste(
    "True positive rate in test labels",
    "= Probability(peak | peak)"),
                     breaks=seq(0, 1, by=0.2))+
  theme(panel.margin=grid::unit(0, "cm"))+
  theme(legend.position=c(1, 0),
        legend.justification=c(1, 0))
pdf.png(gg.roc.one, "figure-roc-test-one.pdf", width=7.7, height=6)

## A small example to demonstrate why ROC curves can be non-monotonic.
test.set <- "H3K36me3_TDH_other"
test.fold <- 3
test.chunk <- dp.peaks.sets[[test.set]][[test.fold]]
chunk.dir <- file.path("chunks", test.chunk)
regions.RData <- file.path(chunk.dir, "regions.RData")
load(regions.RData)
counts.RData <- file.path(chunk.dir, "counts.RData")
load(counts.RData)
counts.dt <- data.table(counts)
counts.dt[, count := coverage]
position <- counts.dt[, seq(min(chromStart), max(chromEnd), l=1000)]
count.range <- counts.dt[, {
  min.chromStart <- min(chromStart)
  max.chromEnd <- max(chromEnd)
  bases <- max.chromEnd - min.chromStart
  bins <- 1000L
  bases.per.bin <- as.integer(bases/bins)
  data.table(min.chromStart, max.chromEnd, bases, bins, bases.per.bin)
}]
downsampled <- counts.dt[, {
  binSum(.SD,
         bin.chromStart=count.range$min.chromStart,
         bin.size=count.range$bases.per.bin,
         n.bins=count.range$bins)
}, by=.(cell.type, sample.id)]
smalldiff <- function(x){
  abs(diff(x)) < 1e-6
}
path.dt <- one(trained.dt)[model!="PeakSeg",]
path.dt[, Model := factor(model, sorted.models$model)]
path.dt[, ModelParam := paste(Model, param.num)]
setkey(path.dt, model, param.num)
path.dt[, same.as.prev := c(FALSE, smalldiff(TPR) & smalldiff(FPR))]
clickable.points <- path.dt[same.as.prev==FALSE,]
parameter.dt <- one(parameter.points)[model!="PeakSeg",]
show.peaks.list <- list()
show.error.list <- list()
model.vec <- clickable.points[!grepl("PeakSeg", model), unique(model)]
peak.cols <- c("sample.id", "chromStart", "chromEnd")
for(model in model.vec){
  trained.RData <- sprintf("%s/peaks/%s.trained.RData", chunk.dir, model)
  load(trained.RData)
  error.RData <- sprintf("%s/error/%s.trained.RData", chunk.dir, model)
  load(error.RData)
  error.by.param <- split(error, error$param.name)
  param.vec <- paste(clickable.points[model, param.num])
  for(param.name in param.vec){
    ModelParam <- paste(model, param.name)
    show.error.list[[ModelParam]] <-
      data.table(ModelParam, error.by.param[[param.name]])
    peaks.df <- peaks[[param.name]]
    if(nrow(peaks.df)){
      show.peaks.list[[ModelParam]] <- 
        data.table(ModelParam, peaks.df[, peak.cols])
    }
  }
}
param.vec <- paste(clickable.points[model=="PeakSegJoint", param.num])
e.names <- names(show.error.list[[1]])
for(param.name in param.vec){
  pred.list <- PeakSegJoint.predictions.list[[test.chunk]][[param.name]]
  ModelParam <- paste("PeakSegJoint", param.name)
  show.peaks.list[[ModelParam]] <-
    data.table(ModelParam,
               pred.list$peaks[, peak.cols])
  show.error.list[[ModelParam]] <-
    data.frame(ModelParam, param.name, pred.list$error.regions)[, e.names]
}
show.peaks <- do.call(rbind, show.peaks.list)
show.error <- do.call(rbind, show.error.list)
type.df <- data.frame(unique(counts.dt[, .(cell.type, sample.id)]))
rownames(type.df) <- type.df$sample.id
show.peaks[, cell.type := type.df[paste(sample.id), "cell.type"]]
show.error[, cell.type := type.df[paste(sample.id), "cell.type"]]
show.stats <- show.error[, data.table(
  fp=sum(fp),
  fn=sum(fn),
  possible.fp=sum(possible.fp),
  possible.fn=sum(possible.tp),
  errors=sum(fp+fn),
  regions=length(fp)
  ), by=ModelParam]
ann.colors <-
  c(noPeaks="#f6f4bf",
    peakStart="#ffafaf",
    peakEnd="#ff4c4c",
    peaks="#a445ee")
chunk_vars <- c("ModelParam")
## TestROC <- list(
##   parameters=data.frame(parameter.dt),
##   roc=data.frame(path.dt))
## save(TestROC, file="~/R/animint/data/TestROC.RData")
## prompt(TestROC, "~/R/animint/man/TestROC.Rd")
viz <- list(
  title="Test ROC curves, predicted peaks and errors",
  roc=ggplot()+
    ggtitle("Test ROC curves, select model")+
    geom_path(aes(FPR, TPR, group=Model, key=Model, color=Model),
              data=path.dt)+
    geom_point(aes(FPR, TPR, color=Model, key=paste(model, parameter),
                   size=parameter,
                   fill=parameter),
               shape=21,
               data=parameter.dt[parameter=="learned",])+
    geom_point(aes(FPR, TPR, color=Model, key=paste(model, parameter),
                   size=parameter,
                   fill=parameter),
               shape=21,
               data=parameter.dt[parameter=="default",])+
    geom_point(aes(FPR, TPR, color=Model,
                   key=ModelParam,
                   clickSelects=ModelParam),
               size=4,
               alpha=0.9,
               data=clickable.points)+
    geom_text(aes(
      0.6, 0.35,
      label=ModelParam,
      showSelected=ModelParam),
              data=show.stats)+
    geom_text(aes(
      0.6, 0.3,
      label=sprintf("%d incorrect labels / %d total", errors, regions),
      showSelected=ModelParam),
              data=show.stats)+
    geom_text(aes(
      0.6, 0.25,
      label=sprintf("%d false positives / %d possible", fp, possible.fp),
      showSelected=ModelParam),
              data=show.stats)+
    geom_text(aes(
      0.6, 0.2,
      label=sprintf("%d false negatives / %d possible", fn, possible.fn),
      showSelected=ModelParam),
              data=show.stats)+
    scale_color_manual(values=algo.colors)+
    scale_fill_manual(values=c(default="black", learned="white"))+
    scale_size_manual(values=c(default=7, learned=10))+
    scale_shape_manual(values=c(default=20, learned=1))+
    coord_equal(xlim=c(0,1), ylim=c(0,1))+
    theme_grey()+
    scale_x_continuous(paste(
      "False positive rate in test labels",
      "= Probability(peak | no peak)"),
                       breaks=seq(0, 1, by=0.2))+
    scale_y_continuous(paste(
      "True positive rate in test labels",
      "= Probability(peak | peak)"),
                       breaks=seq(0, 1, by=0.2))+
    theme(panel.margin=grid::unit(0, "lines")),
  profiles=ggplot()+
    ggtitle(test.chunk)+
    geom_tallrect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
                      key=chromStart,
                      fill=annotation),
                  color="grey",
                  alpha=0.5,
                  data=regions)+
    geom_line(aes(chromStart/1e3, mean),
              color="grey50",
              data=downsampled)+
    ## geom_rect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
    ##               ymin=0, ymax=mean),
    ##           fill="grey",
    ##           size=0.2,
    ##           data=downsampled)+
    coord_cartesian(xlim=count.range[, c(min.chromStart, max.chromEnd)/1e3])+
    geom_tallrect(aes(xmin=chromStart/1e3, xmax=chromEnd/1e3,
                      key=chromStart,
                      showSelected=ModelParam,
                      showSelected2=annotation,
                      showSelected3=status,
                      linetype=status),
                  color="black",
                  chunk_vars=chunk_vars,
                  fill="transparent",
                  data=show.error)+
    geom_segment(aes(chromStart/1e3, 0,
                     showSelected=ModelParam,
                     xend=chromEnd/1e3, yend=0),
                 color="deepskyblue",
                 size=5,
                 chunk_vars=chunk_vars,
                 data=show.peaks)+
    geom_point(aes(chromStart/1e3, 0,
                   showSelected=ModelParam),
               color="deepskyblue",
               chunk_vars=chunk_vars,
               size=5,
               data=show.peaks)+
    scale_y_continuous("aligned read coverage",
                       breaks=function(limits){
                         floor(limits[2])
                       })+
    xlab("position on chromosome (kilo bases = kb)")+
    scale_linetype_manual("error type",
                          limits=c("correct", 
                            "false negative",
                            "false positive"
                                   ),
                          values=c(correct=0,
                            "false negative"=3,
                            "false positive"=1))+
    scale_fill_manual("label", values=ann.colors)+
    theme_bw()+
    theme_animint(height=1000, width=1000)+
    theme(panel.margin=grid::unit(0, "lines"))+
    facet_grid(cell.type + sample.id ~ ., labeller=function(var, val){
      if(var=="sample.id"){
        sub("McGill0", "", val)
      }else{
        sub("skeletal", "", val)
      }
    }, scales="free"),
  first=list(
    ModelParam="PeakSegJoint -0.8",
    Model=c("PeakSegJoint", "macs.broad", "hmcan.broad")))
animint2dir(viz, "figure-roc-test")

## plot with only 3 panels. test fold 1 is fine.
show.sets <- c("srf_NTNU_Gm12878", "H3K36me3_AM_immune", "H3K4me3_TDH_other")
some <- function(dt){
  dt[set.name %in% show.sets & Model!="PeakSegJoint",]
}
desc.vec <- c(
  H3K36me3="broad histone mark",
  H3K4me3="sharp histone mark",
  srf="transcription factor")
gg.test.some <- 
ggplot()+
  geom_point(aes(test.error.percent, Model, color=Model, shape=parameter),
             size=4,
             data=some(parameter.points))+
  scale_shape_manual(values=c(default=20, learned=1))+
  scale_color_manual(values=algo.colors)+
  guides(color="none")+
  scale_x_continuous(
    paste("Test error",
          "(percent incorrect labels),",
          "4 circles = 4 cross validation folds"),
    breaks=seq(0, 80, by=20))+
  facet_grid(. ~ set.name, labeller=function(var, val){
    experiment <- sub("_.*", "", val)
    sprintf("%s (%s)", desc.vec[experiment], experiment)
  })+
  theme_bw()+
  theme(panel.margin=grid::unit(0, "cm"))
pdf.png(gg.test.some, "figure-test-error-dots.pdf", width=9, height=2.5)

mean.points <- parameter.points[, list(
  mean=mean(test.error.percent),
  sd=sd(test.error.percent)
  ), by=.(set.name, Model, parameter)]
gg.test.some <- 
ggplot()+
  geom_segment(aes(mean+sd, Model,
                   xend=mean-sd, yend=Model,
                   color=Model),
               size=1,
               data=some(mean.points))+
  geom_point(aes(mean, Model, color=Model, shape=parameter),
             size=4,
             data=some(mean.points))+
  scale_shape_manual(values=c(default=20, learned=1))+
  scale_color_manual(values=algo.colors)+
  guides(color="none")+
  scale_x_continuous(
    paste("Test error",
          "(percent incorrect labels)"),
    breaks=seq(0, 80, by=20))+
  facet_grid(. ~ set.name, labeller=function(var, val){
    experiment <- sub("_.*", "", val)
    sprintf("%s (%s)", desc.vec[experiment], experiment)
  })+
  theme_bw()+
  theme(panel.margin=grid::unit(0, "cm"))
pdf.png(gg.test.some, "figure-test-error-dots-mean.pdf", width=9, height=2.5)

zero.dt[, Model := factor(model, sorted.models$model)]
one <- function(dt){
  dt[set.name %in% show.sets &
       set.i==1 &
       model!="PeakSegJoint",]
}
gg.roc.some <- ggplot()+
  geom_path(aes(FPR, TPR, group=paste(Model, set.i), color=Model),
            data=one(zero.dt))+
  scale_color_manual(values=algo.colors)+
  scale_fill_manual(values=algo.colors)+
  geom_point(aes(FPR, TPR, color=Model, shape=parameter),
             size=4,
             data=one(parameter.points))+
  scale_shape_manual(values=c(default=20, learned=1))+
  facet_grid(. ~ set.name, labeller=function(var, val){
    experiment <- sub("_.*", "", val)
    sprintf("%s (%s)", desc.vec[experiment], experiment)
  })+
  guides(fill=guide_legend(
           override.aes=list(shape=22, color="black")))+
  coord_equal(xlim=c(-0.05, 0.45))+
  theme_bw()+
  scale_x_continuous(paste(
    "False positive rate in test labels",
    "= Probability(peak | no peak)"),
                     breaks=seq(0, 0.4, by=0.2),
                     labels=c("0", "0.2", "0.4"))+
  scale_y_continuous(paste(
    "True positive rate in test labels",
    "= Probability(peak | peak)"),
                     breaks=seq(0, 1, by=0.2))+
  theme(panel.margin=grid::unit(0, "cm"))+
  theme(legend.position=c(1, 0),
        legend.justification=c(1, 0))
gg.roc.some

pdf.png(gg.roc.some, "figure-roc-test.pdf", width=7.7, height=6)

