works_with_R("3.2.3", data.table="1.9.6", dplyr="0.4.2")

## Read peak files from disk and save them as a list in an RData file.
load("chunks.RData")

## Parse the first occurance of pattern from each of several strings
## using (named) capturing regular expressions, returning a matrix
## (with column names).
str_match_perl <- function(string,pattern){
  stopifnot(is.character(string))
  stopifnot(is.character(pattern))
  stopifnot(length(pattern)==1)
  parsed <- regexpr(pattern,string,perl=TRUE)
  captured.text <- substr(string,parsed,parsed+attr(parsed,"match.length")-1)
  captured.text[captured.text==""] <- NA
  captured.groups <- do.call(rbind,lapply(seq_along(string),function(i){
    st <- attr(parsed,"capture.start")[i,]
    if(is.na(parsed[i]) || parsed[i]==-1)return(rep(NA,length(st)))
    substring(string[i],st,st+attr(parsed,"capture.length")[i,]-1)
  }))
  result <- cbind(captured.text,captured.groups)
  colnames(result) <- c("",attr(parsed,"capture.names"))
  result
}

rseg.files <- Sys.glob("peaks/rseg/*/*.bed")
rseg.pattern <-
  paste0("peaks/rseg/",
         "(?<experiment>.*?)",
         "/",
         "(?<sample_id>.*?)",
         "[.]bed")
rseg.info <- str_match_perl(rseg.files, rseg.pattern)
colnames(rseg.info)[1] <- "path"

## Split data into sets of chunks which each will use the same data
## files, since there are so many data files to read...
chunk.sets <- 
  split(chunks, with(chunks, list(sample.set, experiment)), drop=TRUE)

thresholds <-
  c(0, 10^c(seq(-0.5, 0.5, by=0.1),
            seq(0.5, 2.5, by=0.04)[-1],
            seq(2.5, 3.5, by=0.1)[-1]))

for(set.i in seq_along(chunk.sets)){
  set <- chunk.sets[[set.i]] %>%
    mutate(chunk.dir=file.path("chunks", set.name, chunk.id),
           peaks.dir=file.path(chunk.dir, "peaks"))
  regions.file <- file.path(set$chunk.dir[1], "regions.RData")
  load(regions.file)
  sample.ids <- unique(regions$sample.id)
  is.sample <- rseg.info[,"sample_id"] %in% sample.ids
  is.experiment <- rseg.info[,"experiment"] == set$experiment[1]
  out.files <- file.path(set$peaks.dir, "rseg.default.RData")
  if(all(file.exists(out.files))){
    cmd <- paste("touch", paste(out.files, collapse=" "))
    print(cmd)
    system(cmd)
  }else{
    rseg.cols <- 
      c(chrom="factor",
        chromStart="integer",
        chromEnd="integer",
        state="NULL",
        averageReadCount="NULL",
        sumPosteriorScores="numeric",
        strand="NULL")
    model.files <- rseg.info[is.sample & is.experiment,]
    ## first read all the files from disk.
    peak.list <- list()
    for(file.i in 1:nrow(model.files)){
      info <- model.files[file.i, ]
      cat(sprintf("%4d / %4d files read %s\n",
                  file.i, nrow(model.files),
                  info[["sample_id"]]))
      
      peaks <- 
        fread(info[["path"]], colClasses=as.character(rseg.cols),
              skip=1, header=FALSE, sep="\t")
      setnames(peaks, names(rseg.cols)[rseg.cols!="NULL"])
      setkey(peaks, chrom)
      peak.list[[ info[["sample_id"]] ]] <- peaks
    }
    ## then go through all the chunks and write the peak files.
    for(chunk.i in 1:nrow(set)){
      chunk <- set[chunk.i, ]
      out.file <-
        sprintf("%s/rseg.default.RData",
                chunk$peaks.dir)
      if(!file.exists(out.file)){
        cat(sprintf("%4d / %4d chunks written %s \n",
                    chunk.i, nrow(set),
                    chunk$peaks.dir))
        chunkChrom <- as.character(chunk$chunkChrom)
        chunk.peaks <- list()
        for(sample.id in sample.ids){
          genome.peaks <- peak.list[[sample.id]]
          for(thresh.i in seq_along(thresholds)){
            threshold <- thresholds[[thresh.i]]
            param.name <- as.character(threshold)
            chrom.peaks <- genome.peaks[chunkChrom]
            is.before <- chrom.peaks$chromEnd < chunk$expandStart
            is.after <- chunk$expandEnd < chrom.peaks$chromStart
            in.region <- (!is.before) & (!is.after)
            expand.peaks <- data.frame(chrom.peaks[in.region, ])
            significant.peaks <-
              subset(expand.peaks, sumPosteriorScores > threshold,
                     select=c(chromStart, chromEnd))
            not.na <- !is.na(expand.peaks$chromStart[1])
            to.add <- if(nrow(significant.peaks) && not.na){
              data.frame(sample.id, expand.peaks)
            }else{
              data.frame()
            }
            chunk.peaks[[param.name]] <-
              rbind(chunk.peaks[[param.name]], to.add)
          }
        }
        dir.create(chunk$peaks.dir, showWarnings=FALSE)
        file.params <- list(default="0", trained=names(chunk.peaks))
        for(model.type in names(file.params)){
          param.names <- file.params[[model.type]]
          peaks <- chunk.peaks[param.names]
          out.file <-
            sprintf("%s/rseg.%s.RData",
                    chunk$peaks.dir, model.type)
          save(peaks, file=out.file)
        }
      }#file.exists
    }#chunk.i
  }#file.exists
}
