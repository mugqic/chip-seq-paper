source("packages.R")

load("TF.benchmark.corrected.RData")
load("chunks.RData")

tf.names <- c("srf", "max", "nrsf")
chroms <- paste0("chr", c(1:22, "X", "Y"))

MAX.P.values <-
  sort(c(seq(0.1, 0.5, by=0.0125),
         0.05, 0.01, 0.005, 0.001))

## Check to make sure all triform peak files are present.
g <- 
expand.grid(MAX.P=MAX.P.values,
            tf=tf.names,
            chrom=chroms)
g$f <- with(g, {
  sprintf("TF-benchmark/%s_%s_triform_MAX.P=%f.csv",
          chrom, tf, MAX.P)
})
no.file <- subset(g, !file.exists(f))
stopifnot(nrow(no.file) == 0)

peak.pattern <-
  paste0("(?<chrom>chr.*?)",
         ":",
         "(?<chromStart>[0-9]+?)",
         "-",
         "(?<chromEnd>[0-9]+?)",
         ":")

files.by.tf <- split(g, g$tf)

peaks.list <- list()
error.regions.list <- list()
seconds.list <- list()
for(tf.name in names(files.by.tf)){

  chunk.list <- TF.benchmark.corrected[[tf.name]]
  regions.by.chrom <- list()
  for(chunk.i in seq_along(chunk.list)){
    chunk.name <- names(chunk.list)[[chunk.i]]
    chunk <- chunk.list[[chunk.i]]
    chrom <- paste(chunk$coverage$chrom[1])
    regions.by.chrom[[chrom]][[chunk.name]] <- chunk$corrected.regions
  }

  files.one.tf <- files.by.tf[[tf.name]]

  files.by.chrom <- split(files.one.tf, files.one.tf$chrom)
  for(chrom in names(files.by.chrom)){
    rlist.or.null <- regions.by.chrom[[chrom]]
    if(!is.null(rlist.or.null)){
      chrom.regions <- do.call(rbind, rlist.or.null)

      files.one.chrom <- files.by.chrom[[chrom]]

      for(file.i in 1:nrow(files.one.chrom)){
        file.row <- files.one.chrom[file.i, ]
        param.name <- paste(file.row$MAX.P)
        param.num <- as.numeric(file.row$MAX.P)
        cat(sprintf("%s %s %s\n", tf.name, chrom, param.name))
        seconds.file <- sub(".csv", "_secondsDF.RData", file.row$f)
        load(seconds.file)
        seconds.list[[paste(tf.name, chrom, param.name)]] <- secondsDF
        peak.df <- tryCatch({
          param.df <- read.table(file.row$f)
          peak.mat <- str_match_perl(rownames(param.df), peak.pattern)
          data.frame(chrom=peak.mat[,"chrom"],
                     chromStart=as.integer(peak.mat[,"chromStart"]),
                     chromEnd=as.integer(peak.mat[,"chromEnd"]))
        }, error=function(e){
          Peaks()
        })
        error.df <- PeakErrorChrom(peak.df, chrom.regions)
        error.regions.list[[paste(tf.name, chrom, param.name)]] <-
          data.frame(tf.name, chrom, param.name, param.num,
                     error.df)
        if(nrow(peak.df)){
          peaks.list[[paste(tf.name, chrom, param.name)]] <-
            data.table(tf.name, param.name, param.num,
                       peak.df)
        }
      }#file.i
    }#!is.null(rlist.or.null)
  }#chrom
}#tf.name

all.peaks.dt <- do.call(rbind, peaks.list)
setkey(all.peaks.dt, tf.name, chrom, chromStart, chromEnd)
chunks.dt <- data.table(chunks)[annotator=="NTNU",]
setkey(chunks.dt, experiment, chunkChrom, expandStart, expandEnd)
over.dt <- foverlaps(all.peaks.dt, chunks.dt, nomatch=0L)
over.dt[experiment != tf.name, ]
over.dt[, param.fac := factor(param.name, sort(unique(param.num)))]
stopifnot(!is.na(over.dt$param.fac))
setkey(over.dt, set.name, chunk.id)
setkey(chunks.dt, set.name, chunk.id)

for(chunk.i in 1:nrow(chunks.dt)){
  chunk <- chunks.dt[chunk.i, ]
  peaks.dir <- chunk[, file.path("chunks", set.name, chunk.id, "peaks")]
  cat(sprintf("%d / %d %s\n", chunk.i, nrow(chunks.dt), peaks.dir))
  chunk.peaks <- data.frame(over.dt[chunk, ])
  peaks.by.param <- split(chunk.peaks, chunk.peaks$param.fac)
  sapply(peaks.by.param, nrow)
  param.vec.list <- list(
    trained=names(peaks.by.param),
    default="0.1")
  for(model.type in names(param.vec.list)){
    param.vec <- param.vec.list[[model.type]]
    peaks <- peaks.by.param[param.vec]
    out.RData <- sprintf("%s/triform.%s.RData", peaks.dir, model.type)
    save(peaks, file=out.RData)
  }
}

triform.error <-
  list(error.regions=do.call(rbind, error.regions.list),
       peaks=data.frame(all.peaks.dt),
       seconds=do.call(rbind, seconds.list))

save(triform.error, file="triform.error.RData")
