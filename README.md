This repository contains source files for the figures and text of the
paper "Visual annotations and a supervised learning approach for
evaluating and calibrating ChIP-seq peak detectors" by Toby Dylan
Hocking et al.

* [arXiv pre-print](http://arxiv.org/abs/1409.6209)
* [slides](https://bitbucket.org/mugqic/chip-seq-paper/raw/master/HOCKING-chip-seq-slides.pdf)